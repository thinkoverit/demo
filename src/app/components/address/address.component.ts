import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from "@angular/forms";

import { PincodeService } from "../../services/pincode.service";

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  @Output() formReady = new EventEmitter<FormGroup>()
  @Input() form: FormGroup;

	AddressGroup: FormGroup;
	pinValue: string;

  	constructor(private fb: FormBuilder, private pincodeService: PincodeService) {

  	}

    ngOnInit() {
  		//this.pinValid();
    }
    inItAddress(){
  		let address = this.fb.group({
  			pin: ['', [Validators.required, Validators.pattern('^[0-9 \-\']+'), Validators.minLength(6), Validators.maxLength(6)]
			],
			city: ['', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+'), Validators.minLength(4), Validators.maxLength(20)]],
			state: ['', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+'), Validators.minLength(4), Validators.maxLength(20)]
		    ],
			street: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9#@+& \-\']+'), Validators.minLength(4), Validators.maxLength(20)]
			]
  		});
  		return address;
    } 

    addAddress(){
   		let formArray = <FormArray> this.form.get('userAddresses'); 
  		(formArray.length<5)?(formArray.push(this.inItAddress())):(alert('can not have more than 5 entries'));
    } 
    removeAddress(index){
   		let formArray = <FormArray> this.form.get('userAddresses'); 
  		(formArray.length>1)?(formArray.removeAt(index)):(console.log('cannot remove'));
    } 

	isValid(){
		if(this.form.valid){
			// Emit the form group to the parent 
			this.formReady.emit(this.form);
			console.log(this.formReady);
		}
	}
/*
	pinValid(){
		this.AddressGroup.get('addresses')['controls'][0].get('pin').valueChanges.subscribe(value => {
			this.pinValue = this.AddressGroup.get('addresses')['controls'][0].get('pin').value.slice(0, 1);
			//console.log(this.AddressGroup.get('addresses')['controls'][0].get('pin').value);
			if(this.AddressGroup.get('addresses')['controls'][0].get('pin').valid){
				this.pincodeService.serverRequest(this.pinValue).subscribe((data: any) => {
          		console.log(data);
				this.AddressGroup.get('addresses')['controls'][0].patchValue({ state: data.completed, city: data.completed });
            });
			}
    }); 
	} */
}
