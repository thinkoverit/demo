import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup, FormArray } from '@angular/forms';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    profileForm: FormGroup;
	constructor(private fb: FormBuilder) {
		this.profileForm = this.fb.group({
		    firstName: ['',  
		    	[
			    	Validators.pattern('^[a-zA-Z \-\']+'),
			    	Validators.required,
			    	Validators.minLength(4),
			    	Validators.maxLength(20)
		    	]
		    ],
		    lastName: ['', 
		    	[ 
		    		Validators.pattern('^[a-zA-Z \-\']+'),
		    		Validators.required,
		    		Validators.minLength(4),
			    	Validators.maxLength(20)
		        ]
		    ],
		    userAddresses: this.fb.array([])
	    });
	}
	ngOnInit() {
  	}
  	addressInitialized(formReady) {
    	let formArray = <FormArray> this.profileForm.get('userAddresses'); 
  		formArray.push(formReady);
    	console.log(this.profileForm);
    }
    // Submit method
    onSubmit() {
		console.log(this.profileForm.value);
    }
}