import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PincodeService {

	pinValue: number;
	constructor(private httpClient: HttpClient) { }
	serverRequest(pinValue){
		return this.httpClient.get('https://jsonplaceholder.typicode.com/todos/'+pinValue); 
	}
}
