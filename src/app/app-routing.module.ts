import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileComponent } from './components/profile/profile.component';
import { AddressComponent } from './components/address/address.component';

const routes: Routes = [
	{ path: '', redirectTo: '/register-profile', pathMatch: 'full' },
	{ path: 'register-profile' , component: ProfileComponent},
	{ path: 'address' , component: AddressComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], 
  exports: [RouterModule]
})
export class AppRoutingModule { }